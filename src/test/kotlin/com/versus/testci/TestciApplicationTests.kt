package com.versus.testci

import com.versus.testci.service.Addition
import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class TestciApplicationTests {

	val addition =  Addition();

	@Test
	fun addition_OK() {
		// Given
		val nb1 = 1
		val nb2 = 2

		// When
		val result = addition.add(nb1, nb2);

		// Then
		val expectedResult  = 3;
		assertThat(result).isEqualTo(expectedResult)
	}
}
