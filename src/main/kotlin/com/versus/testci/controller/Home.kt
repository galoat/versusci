package com.versus.testci.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam


@Controller
class GreetingController {
    @GetMapping("/home")
    fun home(): String {
        return "home"
    }
}