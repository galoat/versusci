package com.versus.testci.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class Connect {
    @GetMapping("/connect")
    fun connect(): String {
        return "connect"
    }
}