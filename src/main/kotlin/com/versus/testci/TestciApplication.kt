package com.versus.testci

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestciApplication

fun main(args: Array<String>) {
	runApplication<TestciApplication>(*args)
}
